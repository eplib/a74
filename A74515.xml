<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A74515">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>An ordinance for continuation of one act of Parliament, entituled, An act for redemption of captives. Saturday the 24. of December 1653. Ordered by His Highness the Lord Protector, and His Council, that this ordinance be forthwith printed and published. Hen. Scobell, Clerk of the Council.</title>
    <author>England and Wales. Lord Protector (1653-1658 : O. Cromwell)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A74515 of text xxx in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason E1063_2). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 3 1-bit group-IV TIFF page images.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A74515</idno>
    <idno type="STC">Thomason E1063_2</idno>
    <idno type="STC">ESTC R209528</idno>
    <idno type="EEBO-CITATION">99868404</idno>
    <idno type="PROQUEST">99868404</idno>
    <idno type="VID">169168</idno>
    <idno type="PROQUESTGOID">2240950365</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. A74515)</note>
    <note>Transcribed from: (Early English Books Online ; image set 169168)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 158:E1063[2])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>An ordinance for continuation of one act of Parliament, entituled, An act for redemption of captives. Saturday the 24. of December 1653. Ordered by His Highness the Lord Protector, and His Council, that this ordinance be forthwith printed and published. Hen. Scobell, Clerk of the Council.</title>
      <author>England and Wales. Lord Protector (1653-1658 : O. Cromwell)</author>
      <author>England and Wales. Council of State.</author>
     </titleStmt>
     <extent>[2], 7-8 p.</extent>
     <publicationStmt>
      <publisher>Printed by Hen. Hills, Printer to His Highness the Lord Protector,</publisher>
      <pubPlace>London :</pubPlace>
      <date>MDCLIII. [1653]</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Ransom -- England -- Early works to 1800. -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>An ordinance for continuation of one act of Parliament,:  entituled, An act for redemption of captives. Saturday the 24. of December 1653. Ordered by</ep:title>
    <ep:author>England and Wales. Lord Protector  </ep:author>
    <ep:publicationYear>1653</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>4</ep:pageCount>
    <ep:wordCount>435</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2009-04</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2009-05</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2009-06</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-06</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A74515-e10010">
  <front xml:id="A74515-e10020">
   <div type="title_page" xml:id="A74515-e10030">
    <pb facs="tcp:169168:1" rend="simple:additions" xml:id="A74515-001-a"/>
    <p xml:id="A74515-e10040">
     <w lemma="a" pos="d" xml:id="A74515-001-a-0010">AN</w>
     <w lemma="ordinance" pos="n1" xml:id="A74515-001-a-0020">ORDINANCE</w>
     <w lemma="for" pos="acp" xml:id="A74515-001-a-0030">for</w>
     <w lemma="continuation" pos="n1" xml:id="A74515-001-a-0040">Continuation</w>
     <w lemma="of" pos="acp" xml:id="A74515-001-a-0050">of</w>
     <w lemma="one" pos="crd" xml:id="A74515-001-a-0060">one</w>
     <w lemma="act" pos="n1" xml:id="A74515-001-a-0070">Act</w>
     <w lemma="of" pos="acp" xml:id="A74515-001-a-0080">of</w>
     <w lemma="parliament" pos="n1" xml:id="A74515-001-a-0090">Parliament</w>
     <pc xml:id="A74515-001-a-0100">,</pc>
     <w lemma="entitle" pos="vvn" reg="ENTITLED" xml:id="A74515-001-a-0110">ENTITULED</w>
     <pc xml:id="A74515-001-a-0120">,</pc>
     <hi xml:id="A74515-e10050">
      <w lemma="a" pos="d" xml:id="A74515-001-a-0130">An</w>
      <w lemma="act" pos="n1" xml:id="A74515-001-a-0140">Act</w>
      <w lemma="for" pos="acp" xml:id="A74515-001-a-0150">for</w>
      <w lemma="redemption" pos="n1" xml:id="A74515-001-a-0160">Redemption</w>
      <w lemma="of" pos="acp" xml:id="A74515-001-a-0170">of</w>
      <w lemma="captive" pos="n2-j" xml:id="A74515-001-a-0180">CAPTIVES</w>
      <pc unit="sentence" xml:id="A74515-001-a-0190">.</pc>
     </hi>
    </p>
    <figure xml:id="A74515-e10060">
     <figDesc xml:id="A74515-e10070">blazon of the Commonwealth</figDesc>
    </figure>
    <floatingText xml:id="A74515-e10090">
     <body xml:id="A74515-e10100">
      <div type="license" xml:id="A74515-e10110">
       <opener xml:id="A74515-e10120">
        <dateline xml:id="A74515-e10130">
         <date xml:id="A74515-e10140">
          <w lemma="Saturday" pos="nn1" rend="hi" xml:id="A74515-001-a-0200">Saturday</w>
          <w lemma="the" pos="d" xml:id="A74515-001-a-0210">the</w>
          <w lemma="24." pos="crd" xml:id="A74515-001-a-0220">24.</w>
          <w lemma="of" pos="acp" xml:id="A74515-001-a-0230">of</w>
          <w lemma="December" pos="nn1" rend="hi" xml:id="A74515-001-a-0240">December</w>
          <w lemma="1653." pos="crd" xml:id="A74515-001-a-0250">1653.</w>
          <pc unit="sentence" xml:id="A74515-001-a-0260"/>
         </date>
        </dateline>
       </opener>
       <p xml:id="A74515-e10170">
        <w lemma="order" pos="vvn" xml:id="A74515-001-a-0270">Ordered</w>
        <w lemma="by" pos="acp" xml:id="A74515-001-a-0280">by</w>
        <w lemma="his" pos="po" xml:id="A74515-001-a-0290">His</w>
        <w lemma="highness" pos="n1" xml:id="A74515-001-a-0300">Highness</w>
        <w lemma="the" pos="d" xml:id="A74515-001-a-0310">the</w>
        <w lemma="lord" pos="n1" xml:id="A74515-001-a-0320">Lord</w>
        <w lemma="protector" pos="n1" xml:id="A74515-001-a-0330">Protector</w>
        <pc xml:id="A74515-001-a-0340">,</pc>
        <w lemma="and" pos="cc" xml:id="A74515-001-a-0350">and</w>
        <w lemma="his" pos="po" xml:id="A74515-001-a-0360">His</w>
        <w lemma="council" pos="n1" xml:id="A74515-001-a-0370">Council</w>
        <pc xml:id="A74515-001-a-0380">,</pc>
        <w lemma="that" pos="cs" xml:id="A74515-001-a-0390">That</w>
        <w lemma="this" pos="d" xml:id="A74515-001-a-0400">this</w>
        <w lemma="ordinance" pos="n1" xml:id="A74515-001-a-0410">Ordinance</w>
        <w lemma="be" pos="vvi" xml:id="A74515-001-a-0420">be</w>
        <w lemma="forthwith" pos="av" xml:id="A74515-001-a-0430">forthwith</w>
        <w lemma="print" pos="vvn" xml:id="A74515-001-a-0440">Printed</w>
        <w lemma="and" pos="cc" xml:id="A74515-001-a-0450">and</w>
        <w lemma="publish" pos="vvn" xml:id="A74515-001-a-0460">Published</w>
        <pc unit="sentence" xml:id="A74515-001-a-0470">.</pc>
       </p>
       <closer xml:id="A74515-e10180">
        <signed xml:id="A74515-e10190">
         <w lemma="hen." pos="ab" xml:id="A74515-001-a-0480">HEN.</w>
         <w lemma="scobell" pos="nn1" xml:id="A74515-001-a-0490">SCOBELL</w>
         <pc xml:id="A74515-001-a-0500">,</pc>
         <w lemma="clerk" pos="n1" xml:id="A74515-001-a-0510">Clerk</w>
         <w lemma="of" pos="acp" xml:id="A74515-001-a-0520">of</w>
         <w lemma="the" pos="d" xml:id="A74515-001-a-0530">the</w>
         <w lemma="council" pos="n1" xml:id="A74515-001-a-0540">Council</w>
         <pc unit="sentence" xml:id="A74515-001-a-0550">.</pc>
        </signed>
       </closer>
      </div>
     </body>
    </floatingText>
    <p xml:id="A74515-e10200">
     <w lemma="London" pos="nn1" rend="hi" xml:id="A74515-001-a-0560">London</w>
     <pc xml:id="A74515-001-a-0570">,</pc>
     <w lemma="print" pos="vvn" xml:id="A74515-001-a-0580">Printed</w>
     <w lemma="by" pos="acp" xml:id="A74515-001-a-0590">by</w>
     <hi xml:id="A74515-e10220">
      <w lemma="hen." pos="ab" xml:id="A74515-001-a-0600">Hen.</w>
      <w lemma="hill" pos="n2" xml:id="A74515-001-a-0610">Hills</w>
     </hi>
     <pc rend="follows-hi" xml:id="A74515-001-a-0620">,</pc>
     <w lemma="printer" pos="n1" xml:id="A74515-001-a-0630">Printer</w>
     <w lemma="to" pos="acp" xml:id="A74515-001-a-0640">to</w>
     <w lemma="his" pos="po" xml:id="A74515-001-a-0650">His</w>
     <w lemma="highness" pos="n1" xml:id="A74515-001-a-0660">Highness</w>
     <w lemma="the" pos="d" xml:id="A74515-001-a-0670">the</w>
     <w lemma="lord" pos="n1" xml:id="A74515-001-a-0680">Lord</w>
     <w lemma="protector" pos="n1" xml:id="A74515-001-a-0690">Protector</w>
     <pc xml:id="A74515-001-a-0700">,</pc>
     <w lemma="1653" pos="crd" xml:id="A74515-001-a-0710">MDCLIII</w>
     <pc unit="sentence" xml:id="A74515-001-a-0720">.</pc>
    </p>
   </div>
  </front>
  <body xml:id="A74515-e10230">
   <div type="text" xml:id="A74515-e10240">
    <pb facs="tcp:169168:2" xml:id="A74515-002-a"/>
    <pb facs="tcp:169168:2" n="7" xml:id="A74515-002-b"/>
    <head xml:id="A74515-e10250">
     <figure xml:id="A74515-e10260">
      <figDesc xml:id="A74515-e10270">blazon of the Commonwealth</figDesc>
     </figure>
    </head>
    <head xml:id="A74515-e10280">
     <w lemma="a" pos="d" xml:id="A74515-002-b-0010">AN</w>
     <w lemma="ordinance" pos="n1" xml:id="A74515-002-b-0020">ORDINANCE</w>
     <w lemma="for" pos="acp" xml:id="A74515-002-b-0030">for</w>
     <w lemma="continuation" pos="n1" xml:id="A74515-002-b-0040">Continuation</w>
     <w lemma="of" pos="acp" xml:id="A74515-002-b-0050">of</w>
     <w lemma="one" pos="crd" xml:id="A74515-002-b-0060">one</w>
     <w lemma="act" pos="n1" xml:id="A74515-002-b-0070">Act</w>
     <w lemma="of" pos="acp" xml:id="A74515-002-b-0080">of</w>
     <w lemma="parliament" pos="n1" xml:id="A74515-002-b-0090">Parliament</w>
     <pc xml:id="A74515-002-b-0100">,</pc>
     <w lemma="entitle" pos="vvn" reg="ENTITLED" xml:id="A74515-002-b-0110">ENTITULED</w>
     <pc xml:id="A74515-002-b-0120">,</pc>
     <hi xml:id="A74515-e10290">
      <w lemma="a" pos="d" xml:id="A74515-002-b-0130">An</w>
      <w lemma="act" pos="n1" xml:id="A74515-002-b-0140">Act</w>
      <w lemma="for" pos="acp" xml:id="A74515-002-b-0150">for</w>
      <w lemma="redemption" pos="n1" xml:id="A74515-002-b-0160">Redemption</w>
      <w lemma="of" pos="acp" xml:id="A74515-002-b-0170">of</w>
      <w lemma="captive" pos="n2-j" xml:id="A74515-002-b-0180">Captives</w>
      <pc unit="sentence" xml:id="A74515-002-b-0190">.</pc>
     </hi>
    </head>
    <p xml:id="A74515-e10300">
     <w lemma="be" pos="vvb" rend="decorinit" xml:id="A74515-002-b-0200">BE</w>
     <w lemma="it" pos="pn" xml:id="A74515-002-b-0210">it</w>
     <w lemma="ordain" pos="vvn" xml:id="A74515-002-b-0220">Ordained</w>
     <w lemma="by" pos="acp" xml:id="A74515-002-b-0230">by</w>
     <w lemma="his" pos="po" xml:id="A74515-002-b-0240">His</w>
     <w lemma="highness" pos="n1" xml:id="A74515-002-b-0250">Highness</w>
     <w lemma="the" pos="d" xml:id="A74515-002-b-0260">the</w>
     <w lemma="lord" pos="n1" xml:id="A74515-002-b-0270">Lord</w>
     <w lemma="protector" pos="n1" xml:id="A74515-002-b-0280">Protector</w>
     <pc xml:id="A74515-002-b-0290">,</pc>
     <w lemma="with" pos="acp" xml:id="A74515-002-b-0300">with</w>
     <w lemma="the" pos="d" xml:id="A74515-002-b-0310">the</w>
     <w lemma="advice" pos="n1" xml:id="A74515-002-b-0320">advice</w>
     <w lemma="and" pos="cc" xml:id="A74515-002-b-0330">and</w>
     <w lemma="consent" pos="n1" xml:id="A74515-002-b-0340">consent</w>
     <w lemma="of" pos="acp" xml:id="A74515-002-b-0350">of</w>
     <w lemma="his" pos="po" xml:id="A74515-002-b-0360">his</w>
     <w lemma="council" pos="n1" xml:id="A74515-002-b-0370">Council</w>
     <pc xml:id="A74515-002-b-0380">,</pc>
     <w lemma="that" pos="cs" xml:id="A74515-002-b-0390">That</w>
     <w lemma="one" pos="pi" xml:id="A74515-002-b-0400">one</w>
     <w lemma="act" pos="n1" xml:id="A74515-002-b-0410">Act</w>
     <w lemma="of" pos="acp" xml:id="A74515-002-b-0420">of</w>
     <w lemma="parliament" pos="n1" xml:id="A74515-002-b-0430">Parliament</w>
     <pc xml:id="A74515-002-b-0440">,</pc>
     <w lemma="entitle" pos="vvn" reg="entitled" xml:id="A74515-002-b-0450">entituled</w>
     <pc xml:id="A74515-002-b-0460">,</pc>
     <hi xml:id="A74515-e10310">
      <w lemma="a" pos="d" xml:id="A74515-002-b-0470">An</w>
      <w lemma="act" pos="n1" xml:id="A74515-002-b-0480">Act</w>
      <w lemma="for" pos="acp" xml:id="A74515-002-b-0490">for</w>
      <w lemma="the" pos="d" xml:id="A74515-002-b-0500">the</w>
      <w lemma="redemption" pos="n1" xml:id="A74515-002-b-0510">Redemption</w>
      <w lemma="of" pos="acp" xml:id="A74515-002-b-0520">of</w>
      <w lemma="captive" pos="n2-j" xml:id="A74515-002-b-0530">Captives</w>
      <pc xml:id="A74515-002-b-0540">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A74515-002-b-0550">Printed</w>
     <w lemma="by" pos="acp" xml:id="A74515-002-b-0560">by</w>
     <w lemma="order" pos="n1" xml:id="A74515-002-b-0570">Order</w>
     <w lemma="of" pos="acp" xml:id="A74515-002-b-0580">of</w>
     <w lemma="parliament" pos="n1" xml:id="A74515-002-b-0590">Parliament</w>
     <w lemma="of" pos="acp" xml:id="A74515-002-b-0600">of</w>
     <w lemma="the" pos="d" xml:id="A74515-002-b-0610">the</w>
     <w lemma="six" pos="crd" xml:id="A74515-002-b-0620">six</w>
     <w lemma="and" pos="cc" xml:id="A74515-002-b-0630">and</w>
     <w lemma="twenty" pos="ord" xml:id="A74515-002-b-0640">twentieth</w>
     <w lemma="day" pos="n1" xml:id="A74515-002-b-0650">day</w>
     <w lemma="of" pos="acp" xml:id="A74515-002-b-0660">of</w>
     <w lemma="march" pos="n1" rend="hi" xml:id="A74515-002-b-0670">March</w>
     <w lemma="one" pos="crd" xml:id="A74515-002-b-0680">one</w>
     <w lemma="thousand" pos="crd" xml:id="A74515-002-b-0690">thousand</w>
     <w lemma="six" pos="crd" xml:id="A74515-002-b-0700">six</w>
     <pb facs="tcp:169168:3" n="8" xml:id="A74515-003-a"/>
     <w lemma="hundred" pos="crd" xml:id="A74515-003-a-0010">hundred</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-0020">and</w>
     <w lemma="fifty" pos="crd" xml:id="A74515-003-a-0030">fifty</w>
     <pc xml:id="A74515-003-a-0040">,</pc>
     <w lemma="for" pos="acp" xml:id="A74515-003-a-0050">for</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-0060">and</w>
     <w lemma="concern" pos="vvg" xml:id="A74515-003-a-0070">concerning</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-0080">the</w>
     <w lemma="payment" pos="n1" xml:id="A74515-003-a-0090">payment</w>
     <w lemma="of" pos="acp" xml:id="A74515-003-a-0100">of</w>
     <w lemma="one" pos="crd" xml:id="A74515-003-a-0110">one</w>
     <w lemma="four" pos="ord" xml:id="A74515-003-a-0120">fourth</w>
     <w lemma="part" pos="n1" xml:id="A74515-003-a-0130">part</w>
     <w lemma="of" pos="acp" xml:id="A74515-003-a-0140">of</w>
     <w lemma="one" pos="crd" xml:id="A74515-003-a-0150">one</w>
     <foreign xml:id="A74515-e10330" xml:lang="lat">
      <w lemma="n/a" pos="fla" xml:id="A74515-003-a-0160">per</w>
      <w lemma="n/a" pos="fla" xml:id="A74515-003-a-0170">Centum</w>
     </foreign>
     <pc rend="follows-hi" xml:id="A74515-003-a-0180">,</pc>
     <w lemma="be" pos="vvg" xml:id="A74515-003-a-0190">being</w>
     <w lemma="one" pos="crd" xml:id="A74515-003-a-0200">one</w>
     <w lemma="shilling" pos="n1" xml:id="A74515-003-a-0210">shilling</w>
     <w lemma="for" pos="acp" xml:id="A74515-003-a-0220">for</w>
     <w lemma="every" pos="d" xml:id="A74515-003-a-0230">every</w>
     <w lemma="twenty" pos="crd" xml:id="A74515-003-a-0240">twenty</w>
     <w lemma="shilling" pos="n2" xml:id="A74515-003-a-0250">shillings</w>
     <w lemma="pay" pos="vvn" xml:id="A74515-003-a-0260">paid</w>
     <w lemma="for" pos="acp" xml:id="A74515-003-a-0270">for</w>
     <w lemma="custom" pos="n1" xml:id="A74515-003-a-0280">Custom</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-0290">and</w>
     <w lemma="subsidy" pos="n1" xml:id="A74515-003-a-0300">Subsidy</w>
     <w lemma="according" pos="j" xml:id="A74515-003-a-0310">according</w>
     <w lemma="to" pos="acp" xml:id="A74515-003-a-0320">to</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-0330">the</w>
     <w lemma="now" pos="av" xml:id="A74515-003-a-0340">now</w>
     <w lemma="book" pos="n1" xml:id="A74515-003-a-0350">Book</w>
     <w lemma="of" pos="acp" xml:id="A74515-003-a-0360">of</w>
     <w lemma="rate" pos="n2" xml:id="A74515-003-a-0370">Rates</w>
     <pc xml:id="A74515-003-a-0380">,</pc>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-0390">and</w>
     <w lemma="all" pos="d" xml:id="A74515-003-a-0400">all</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-0410">and</w>
     <w lemma="every" pos="d" xml:id="A74515-003-a-0420">every</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-0430">the</w>
     <w lemma="clause" pos="n2" xml:id="A74515-003-a-0440">Clauses</w>
     <pc xml:id="A74515-003-a-0450">,</pc>
     <w lemma="forfeiture" pos="n2" xml:id="A74515-003-a-0460">Forfeitures</w>
     <pc xml:id="A74515-003-a-0470">,</pc>
     <w lemma="penalty" pos="n2" xml:id="A74515-003-a-0480">Penalties</w>
     <pc xml:id="A74515-003-a-0490">,</pc>
     <w lemma="prouiso" pos="n2" reg="Prouisoes" xml:id="A74515-003-a-0500">Provisoes</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-0510">and</w>
     <w lemma="power" pos="n2" xml:id="A74515-003-a-0520">Powers</w>
     <w lemma="therein" pos="av" xml:id="A74515-003-a-0530">therein</w>
     <w lemma="contain" pos="vvn" xml:id="A74515-003-a-0540">contained</w>
     <pc xml:id="A74515-003-a-0550">,</pc>
     <w lemma="in" pos="acp" xml:id="A74515-003-a-0560">in</w>
     <w lemma="reference" pos="n1" xml:id="A74515-003-a-0570">reference</w>
     <w lemma="to" pos="acp" xml:id="A74515-003-a-0580">to</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-0590">the</w>
     <w lemma="collect" pos="vvg" xml:id="A74515-003-a-0600">collecting</w>
     <pc xml:id="A74515-003-a-0610">,</pc>
     <w lemma="receive" pos="vvg" xml:id="A74515-003-a-0620">receiving</w>
     <pc xml:id="A74515-003-a-0630">,</pc>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-0640">and</w>
     <w lemma="due" pos="j" xml:id="A74515-003-a-0650">due</w>
     <w lemma="payment" pos="n1" xml:id="A74515-003-a-0660">payment</w>
     <w lemma="of" pos="acp" xml:id="A74515-003-a-0670">of</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-0680">the</w>
     <w lemma="say" pos="j-vn" xml:id="A74515-003-a-0690">said</w>
     <w lemma="duty" pos="n1" xml:id="A74515-003-a-0700">Duty</w>
     <w lemma="of" pos="acp" xml:id="A74515-003-a-0710">of</w>
     <w lemma="one" pos="crd" xml:id="A74515-003-a-0720">one</w>
     <foreign xml:id="A74515-e10340" xml:lang="lat">
      <w lemma="n/a" pos="fla" xml:id="A74515-003-a-0730">per</w>
      <w lemma="n/a" pos="fla" xml:id="A74515-003-a-0740">Centum</w>
     </foreign>
     <pc rend="follows-hi" xml:id="A74515-003-a-0750">,</pc>
     <w lemma="in" pos="acp" xml:id="A74515-003-a-0760">in</w>
     <w lemma="such" pos="d" xml:id="A74515-003-a-0770">such</w>
     <w lemma="sort" pos="n1" xml:id="A74515-003-a-0780">sort</w>
     <w lemma="as" pos="acp" xml:id="A74515-003-a-0790">as</w>
     <w lemma="by" pos="acp" xml:id="A74515-003-a-0800">by</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-0810">the</w>
     <w lemma="say" pos="j-vn" xml:id="A74515-003-a-0820">said</w>
     <w lemma="act" pos="n1" xml:id="A74515-003-a-0830">Act</w>
     <w lemma="be" pos="vvz" xml:id="A74515-003-a-0840">is</w>
     <w lemma="appoint" pos="vvn" xml:id="A74515-003-a-0850">appointed</w>
     <pc xml:id="A74515-003-a-0860">,</pc>
     <w lemma="be" pos="vvb" xml:id="A74515-003-a-0870">be</w>
     <w lemma="continue" pos="vvn" xml:id="A74515-003-a-0880">continued</w>
     <w lemma="from" pos="acp" xml:id="A74515-003-a-0890">from</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-0900">and</w>
     <w lemma="after" pos="acp" xml:id="A74515-003-a-0910">after</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-0920">the</w>
     <w lemma="twenty" pos="crd" xml:id="A74515-003-a-0930">twenty</w>
     <w lemma="six" pos="ord" reg="sixth" xml:id="A74515-003-a-0940">sixt</w>
     <w lemma="day" pos="n1" xml:id="A74515-003-a-0950">day</w>
     <w lemma="of" pos="acp" xml:id="A74515-003-a-0960">of</w>
     <w lemma="December" pos="nn1" rend="hi" xml:id="A74515-003-a-0970">December</w>
     <pc xml:id="A74515-003-a-0980">,</pc>
     <w lemma="one" pos="crd" xml:id="A74515-003-a-0990">one</w>
     <w lemma="thousand" pos="crd" xml:id="A74515-003-a-1000">thousand</w>
     <w lemma="six" pos="crd" xml:id="A74515-003-a-1010">six</w>
     <w lemma="hundred" pos="crd" xml:id="A74515-003-a-1020">hundred</w>
     <w lemma="fifty" pos="crd" xml:id="A74515-003-a-1030">fifty</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-1040">and</w>
     <w lemma="three" pos="crd" xml:id="A74515-003-a-1050">three</w>
     <pc xml:id="A74515-003-a-1060">,</pc>
     <w lemma="until" pos="acp" xml:id="A74515-003-a-1070">until</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-1080">the</w>
     <w lemma="three" pos="ord" xml:id="A74515-003-a-1090">third</w>
     <w lemma="day" pos="n1" xml:id="A74515-003-a-1100">day</w>
     <w lemma="of" pos="acp" xml:id="A74515-003-a-1110">of</w>
     <w lemma="October" pos="nn1" rend="hi" xml:id="A74515-003-a-1120">October</w>
     <pc xml:id="A74515-003-a-1130">,</pc>
     <w lemma="which" pos="crq" xml:id="A74515-003-a-1140">which</w>
     <w lemma="shall" pos="vmb" xml:id="A74515-003-a-1150">shall</w>
     <w lemma="be" pos="vvi" xml:id="A74515-003-a-1160">be</w>
     <w lemma="in" pos="acp" xml:id="A74515-003-a-1170">in</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-1180">the</w>
     <w lemma="year" pos="n1" xml:id="A74515-003-a-1190">year</w>
     <w lemma="one" pos="crd" xml:id="A74515-003-a-1200">one</w>
     <w lemma="thousand" pos="crd" xml:id="A74515-003-a-1210">thousand</w>
     <w lemma="six" pos="crd" xml:id="A74515-003-a-1220">six</w>
     <w lemma="hundred" pos="crd" xml:id="A74515-003-a-1230">hundred</w>
     <w lemma="fifty" pos="crd" xml:id="A74515-003-a-1240">fifty</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-1250">and</w>
     <w lemma="four" pos="crd" xml:id="A74515-003-a-1260">four</w>
     <pc xml:id="A74515-003-a-1270">,</pc>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-1280">and</w>
     <w lemma="no" pos="dx" xml:id="A74515-003-a-1290">no</w>
     <w lemma="long" pos="avc-j" xml:id="A74515-003-a-1300">longer</w>
     <pc xml:id="A74515-003-a-1310">;</pc>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-1320">and</w>
     <w lemma="that" pos="cs" xml:id="A74515-003-a-1330">that</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-1340">the</w>
     <w lemma="commissioner" pos="n2" xml:id="A74515-003-a-1350">Commissioners</w>
     <w lemma="for" pos="acp" xml:id="A74515-003-a-1360">for</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-1370">the</w>
     <w lemma="custom" pos="n2" xml:id="A74515-003-a-1380">Customs</w>
     <w lemma="for" pos="acp" xml:id="A74515-003-a-1390">for</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-1400">the</w>
     <w lemma="time" pos="n1" xml:id="A74515-003-a-1410">time</w>
     <w lemma="be" pos="vvg" xml:id="A74515-003-a-1420">being</w>
     <pc xml:id="A74515-003-a-1430">,</pc>
     <w lemma="their" pos="po" xml:id="A74515-003-a-1440">their</w>
     <w lemma="deputy" pos="n1" xml:id="A74515-003-a-1450">Deputy</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-1460">and</w>
     <w lemma="deputy" pos="n2" xml:id="A74515-003-a-1470">Deputies</w>
     <pc xml:id="A74515-003-a-1480">,</pc>
     <w lemma="do" pos="vvb" xml:id="A74515-003-a-1490">do</w>
     <w lemma="receive" pos="vvi" xml:id="A74515-003-a-1500">receive</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-1510">and</w>
     <w lemma="collect" pos="vvi" xml:id="A74515-003-a-1520">collect</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-1530">the</w>
     <w lemma="same" pos="d" xml:id="A74515-003-a-1540">same</w>
     <pc xml:id="A74515-003-a-1550">,</pc>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-1560">and</w>
     <w lemma="shall" pos="vmb" xml:id="A74515-003-a-1570">shall</w>
     <w lemma="pay" pos="vvi" xml:id="A74515-003-a-1580">pay</w>
     <w lemma="out" pos="av" xml:id="A74515-003-a-1590">out</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-1600">and</w>
     <w lemma="employ" pos="vvi" reg="employ" xml:id="A74515-003-a-1610">imploy</w>
     <w lemma="all" pos="d" xml:id="A74515-003-a-1620">all</w>
     <w lemma="such" pos="d" xml:id="A74515-003-a-1630">such</w>
     <w lemma="sum" pos="n1" xml:id="A74515-003-a-1640">sum</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-1650">and</w>
     <w lemma="sum" pos="n2" xml:id="A74515-003-a-1660">sums</w>
     <w lemma="of" pos="acp" xml:id="A74515-003-a-1670">of</w>
     <w lemma="money" pos="n1" xml:id="A74515-003-a-1680">money</w>
     <pc xml:id="A74515-003-a-1690">,</pc>
     <w lemma="as" pos="acp" xml:id="A74515-003-a-1700">as</w>
     <w lemma="have" pos="vvi" xml:id="A74515-003-a-1710">have</w>
     <w lemma="be" pos="vvn" xml:id="A74515-003-a-1720">been</w>
     <w lemma="or" pos="cc" xml:id="A74515-003-a-1730">or</w>
     <w lemma="shall" pos="vmb" xml:id="A74515-003-a-1740">shall</w>
     <w lemma="be" pos="vvi" xml:id="A74515-003-a-1750">be</w>
     <w lemma="by" pos="acp" xml:id="A74515-003-a-1760">by</w>
     <w lemma="they" pos="pno" xml:id="A74515-003-a-1770">them</w>
     <w lemma="receive" pos="vvn" xml:id="A74515-003-a-1780">received</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-1790">and</w>
     <w lemma="collect" pos="vvn" xml:id="A74515-003-a-1800">collected</w>
     <w lemma="by" pos="acp" xml:id="A74515-003-a-1810">by</w>
     <w lemma="virtue" pos="n1" reg="virtue" xml:id="A74515-003-a-1820">vertue</w>
     <w lemma="of" pos="acp" xml:id="A74515-003-a-1830">of</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-1840">the</w>
     <w lemma="say" pos="j-vn" xml:id="A74515-003-a-1850">said</w>
     <w lemma="beformentioned" pos="n1" reg="beformentioned" xml:id="A74515-003-a-1860">befor-mentioned</w>
     <w lemma="act" pos="n1" xml:id="A74515-003-a-1870">Act</w>
     <pc xml:id="A74515-003-a-1880">,</pc>
     <w lemma="or" pos="cc" xml:id="A74515-003-a-1890">or</w>
     <w lemma="any" pos="d" xml:id="A74515-003-a-1900">any</w>
     <w lemma="subsequent" pos="j" xml:id="A74515-003-a-1910">subsequent</w>
     <w lemma="act" pos="n1" xml:id="A74515-003-a-1920">Act</w>
     <w lemma="for" pos="acp" xml:id="A74515-003-a-1930">for</w>
     <w lemma="continuation" pos="n1" xml:id="A74515-003-a-1940">continuation</w>
     <w lemma="thereof" pos="av" xml:id="A74515-003-a-1950">thereof</w>
     <pc xml:id="A74515-003-a-1960">,</pc>
     <w lemma="or" pos="cc" xml:id="A74515-003-a-1970">or</w>
     <w lemma="by" pos="acp" xml:id="A74515-003-a-1980">by</w>
     <w lemma="virtue" pos="n1" reg="virtue" xml:id="A74515-003-a-1990">vertue</w>
     <w lemma="of" pos="acp" xml:id="A74515-003-a-2000">of</w>
     <w lemma="this" pos="d" xml:id="A74515-003-a-2010">this</w>
     <w lemma="present" pos="j" xml:id="A74515-003-a-2020">present</w>
     <w lemma="ordinance" pos="n1" xml:id="A74515-003-a-2030">Ordinance</w>
     <pc xml:id="A74515-003-a-2040">,</pc>
     <w lemma="in" pos="acp" xml:id="A74515-003-a-2050">in</w>
     <w lemma="such" pos="d" xml:id="A74515-003-a-2060">such</w>
     <w lemma="sort" pos="n1" xml:id="A74515-003-a-2070">sort</w>
     <w lemma="as" pos="acp" xml:id="A74515-003-a-2080">as</w>
     <w lemma="his" pos="po" xml:id="A74515-003-a-2090">His</w>
     <w lemma="highness" pos="n1" xml:id="A74515-003-a-2100">Highness</w>
     <pc xml:id="A74515-003-a-2110">,</pc>
     <w lemma="with" pos="acp" xml:id="A74515-003-a-2120">with</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-2130">the</w>
     <w lemma="advice" pos="n1" xml:id="A74515-003-a-2140">advice</w>
     <w lemma="of" pos="acp" xml:id="A74515-003-a-2150">of</w>
     <w lemma="his" pos="po" xml:id="A74515-003-a-2160">his</w>
     <w lemma="council" pos="n1" xml:id="A74515-003-a-2170">Council</w>
     <pc xml:id="A74515-003-a-2180">,</pc>
     <w lemma="or" pos="cc" xml:id="A74515-003-a-2190">or</w>
     <w lemma="such" pos="d" xml:id="A74515-003-a-2200">such</w>
     <w lemma="person" pos="n1" xml:id="A74515-003-a-2210">person</w>
     <w lemma="or" pos="cc" xml:id="A74515-003-a-2220">or</w>
     <w lemma="person" pos="n2" xml:id="A74515-003-a-2230">persons</w>
     <w lemma="as" pos="acp" xml:id="A74515-003-a-2240">as</w>
     <w lemma="shall" pos="vmb" xml:id="A74515-003-a-2250">shall</w>
     <w lemma="be" pos="vvi" xml:id="A74515-003-a-2260">be</w>
     <w lemma="appoint" pos="vvn" xml:id="A74515-003-a-2270">appointed</w>
     <w lemma="thereunto" pos="av" xml:id="A74515-003-a-2280">thereunto</w>
     <pc xml:id="A74515-003-a-2290">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A74515-003-a-2300">shall</w>
     <w lemma="order" pos="vvi" xml:id="A74515-003-a-2310">order</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-2320">and</w>
     <w lemma="direct" pos="vvi" xml:id="A74515-003-a-2330">direct</w>
     <pc xml:id="A74515-003-a-2340">,</pc>
     <w lemma="who" pos="crq" xml:id="A74515-003-a-2350">whose</w>
     <w lemma="order" pos="n1" xml:id="A74515-003-a-2360">Order</w>
     <w lemma="from" pos="acp" xml:id="A74515-003-a-2370">from</w>
     <w lemma="time" pos="n1" xml:id="A74515-003-a-2380">time</w>
     <w lemma="to" pos="acp" xml:id="A74515-003-a-2390">to</w>
     <w lemma="time" pos="n1" xml:id="A74515-003-a-2400">time</w>
     <w lemma="shall" pos="vmb" xml:id="A74515-003-a-2410">shall</w>
     <w lemma="be" pos="vvi" xml:id="A74515-003-a-2420">be</w>
     <w lemma="a" pos="d" xml:id="A74515-003-a-2430">a</w>
     <w lemma="sufficient" pos="j" xml:id="A74515-003-a-2440">sufficient</w>
     <w lemma="discharge" pos="n1" xml:id="A74515-003-a-2450">discharge</w>
     <w lemma="unto" pos="acp" xml:id="A74515-003-a-2460">unto</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-2470">the</w>
     <w lemma="say" pos="j-vn" xml:id="A74515-003-a-2480">said</w>
     <w lemma="commissioner" pos="n2" xml:id="A74515-003-a-2490">Commissioners</w>
     <w lemma="for" pos="acp" xml:id="A74515-003-a-2500">for</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-2510">the</w>
     <w lemma="custom" pos="n2" xml:id="A74515-003-a-2520">Customs</w>
     <pc xml:id="A74515-003-a-2530">,</pc>
     <w lemma="for" pos="acp" xml:id="A74515-003-a-2540">for</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-2550">the</w>
     <w lemma="same" pos="d" xml:id="A74515-003-a-2560">same</w>
     <pc unit="sentence" xml:id="A74515-003-a-2570">.</pc>
    </p>
   </div>
   <div type="license" xml:id="A74515-e10370">
    <opener xml:id="A74515-e10380">
     <dateline xml:id="A74515-e10390">
      <date xml:id="A74515-e10400">
       <w lemma="Saturday" pos="nn1" rend="hi" xml:id="A74515-003-a-2580">Saturday</w>
       <w lemma="the" pos="d" xml:id="A74515-003-a-2590">the</w>
       <w lemma="24." pos="crd" xml:id="A74515-003-a-2600">24.</w>
       <w lemma="of" pos="acp" xml:id="A74515-003-a-2610">of</w>
       <w lemma="December" pos="nn1" rend="hi" xml:id="A74515-003-a-2620">December</w>
       <w lemma="1653." pos="crd" xml:id="A74515-003-a-2630">1653.</w>
       <pc unit="sentence" xml:id="A74515-003-a-2640"/>
      </date>
     </dateline>
    </opener>
    <p xml:id="A74515-e10430">
     <w lemma="order" pos="vvn" xml:id="A74515-003-a-2650">Ordered</w>
     <w lemma="by" pos="acp" xml:id="A74515-003-a-2660">by</w>
     <w lemma="his" pos="po" xml:id="A74515-003-a-2670">his</w>
     <w lemma="highness" pos="n1" xml:id="A74515-003-a-2680">Highness</w>
     <w lemma="the" pos="d" xml:id="A74515-003-a-2690">the</w>
     <w lemma="lord" pos="n1" xml:id="A74515-003-a-2700">Lord</w>
     <w lemma="protector" pos="n1" xml:id="A74515-003-a-2710">Protector</w>
     <pc xml:id="A74515-003-a-2720">,</pc>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-2730">and</w>
     <w lemma="his" pos="po" xml:id="A74515-003-a-2740">his</w>
     <w lemma="council" pos="n1" xml:id="A74515-003-a-2750">Council</w>
     <pc xml:id="A74515-003-a-2760">,</pc>
     <w lemma="that" pos="cs" xml:id="A74515-003-a-2770">That</w>
     <w lemma="this" pos="d" xml:id="A74515-003-a-2780">this</w>
     <w lemma="ordinance" pos="n1" xml:id="A74515-003-a-2790">Ordinance</w>
     <w lemma="be" pos="vvi" xml:id="A74515-003-a-2800">be</w>
     <w lemma="forthwith" pos="av" xml:id="A74515-003-a-2810">forthwith</w>
     <w lemma="print" pos="vvn" xml:id="A74515-003-a-2820">Printed</w>
     <w lemma="and" pos="cc" xml:id="A74515-003-a-2830">and</w>
     <w lemma="publish" pos="vvn" xml:id="A74515-003-a-2840">Published</w>
     <pc unit="sentence" xml:id="A74515-003-a-2850">.</pc>
    </p>
    <closer xml:id="A74515-e10440">
     <signed xml:id="A74515-e10450">
      <hi xml:id="A74515-e10460">
       <w lemma="Henry" pos="nn1" xml:id="A74515-003-a-2860">Henry</w>
       <w lemma="scobell" pos="nn1" xml:id="A74515-003-a-2870">Scobell</w>
      </hi>
      <pc rend="follows-hi" xml:id="A74515-003-a-2880">,</pc>
      <w lemma="clerk" pos="n1" xml:id="A74515-003-a-2890">Clerk</w>
      <w lemma="of" pos="acp" xml:id="A74515-003-a-2900">of</w>
      <w lemma="the" pos="d" xml:id="A74515-003-a-2910">the</w>
      <w lemma="council" pos="n1" xml:id="A74515-003-a-2920">Council</w>
      <pc unit="sentence" xml:id="A74515-003-a-2930">.</pc>
     </signed>
    </closer>
   </div>
  </body>
 </text>
</TEI>
